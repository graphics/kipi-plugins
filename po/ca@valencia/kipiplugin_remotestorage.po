# Translation of kipiplugin_remotestorage.po to Catalan (Valencian)
# Copyright (C) 2010-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2010, 2012, 2015, 2016.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2012, 2015, 2017, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kipi-plugins\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2023-02-22 21:53+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#: KioExportWidget.cpp:61
#, kde-format
msgid "Target location: "
msgstr "Ubicació de la destinació: "

#: KioExportWidget.cpp:62
#, kde-format
msgid ""
"Sets the target address to upload the images to. This can be any address as "
"used in Dolphin or Konqueror, e.g. ftp://my.server.org/sub/folder."
msgstr ""
"Indica l'adreça de la destinació a on s'han de pujar les imatges. Pot ser "
"una adreça emprada en Dolphin o Konqueror, p. ex., ftp://elmeu.servidor.org/"
"sub/carpeta."

#: KioExportWidget.cpp:66
#, kde-format
msgid "Select target location..."
msgstr "Seleccioneu la ubicació de la destinació…"

#: KioExportWidget.cpp:72
#, kde-format
msgid "This is the list of images to upload to the specified target."
msgstr ""
"Esta és la llista de les imatges que es pujaran a la destinació especificada."

#: KioExportWidget.cpp:134
#, kde-format
msgid "Select target..."
msgstr "Seleccioneu la destinació…"

#: KioExportWidget.cpp:135
#, kde-format
msgid "All Files (*)"
msgstr "Tots els fitxers (*)"

#: KioExportWidget.cpp:155
#, kde-format
msgid "<not selected>"
msgstr "<no seleccionat>"

#: KioExportWindow.cpp:64
#, kde-format
msgid "Export to Remote Storage"
msgstr "Exporteu cap a un emmagatzematge remot"

#: KioExportWindow.cpp:67
#, kde-format
msgid "Start export"
msgstr "Inicia l'exportació"

#: KioExportWindow.cpp:68
#, kde-format
msgid "Start export to the specified target"
msgstr "Inicia l'exportació cap a la destinació especificada"

#: KioExportWindow.cpp:84
#, kde-format
msgid "Export to remote storage"
msgstr "Exporta cap a un emmagatzematge remot"

#: KioExportWindow.cpp:85
#, kde-format
msgid "A tool to export images over network using KIO-Slave"
msgstr "Una eina per a exportar imatges per xarxa emprant l'esclau KIO"

#: KioExportWindow.cpp:86 KioImportWindow.cpp:83
#, kde-format
msgid "(c) 2009, Johannes Wienke"
msgstr "(c) 2009, Johannes Wienke"

#: KioExportWindow.cpp:88 KioImportWindow.cpp:85
#, kde-format
msgid "Johannes Wienke"
msgstr "Johannes Wienke"

#: KioExportWindow.cpp:89 KioImportWindow.cpp:86
#, kde-format
msgid "Developer and maintainer"
msgstr "Desenvolupador i mantenidor"

#: KioExportWindow.cpp:198
#, kde-format
msgid "Upload not completed"
msgstr "No s'ha completat la pujada"

#: KioExportWindow.cpp:199
#, kde-format
msgid ""
"Some of the images have not been transferred and are still in the list. You "
"can retry to export these images now."
msgstr ""
"Algunes de les imatges no s'han transferit i encara es troben en la llista. "
"Ara podeu tornar a intentar exportar-les."

#: KioImportWidget.cpp:52
#, kde-format
msgid "This is the list of images to import into the current album."
msgstr ""
"Esta és la llista de les imatges que s'importaran cap a dins de l'àlbum "
"actual."

#: KioImportWindow.cpp:60
#, kde-format
msgid "Import from Remote Storage"
msgstr "Importació des d'un emmagatzematge remot"

#: KioImportWindow.cpp:64
#, kde-format
msgid "Start import"
msgstr "Inicia la importació"

#: KioImportWindow.cpp:65
#, kde-format
msgid "Start importing the specified images into the currently selected album."
msgstr ""
"Inicia la importació de les imatges especificades en l'àlbum actualment "
"seleccionat."

#: KioImportWindow.cpp:81
#, kde-format
msgid "Import from remote storage"
msgstr "Importa des d'un emmagatzematge remot"

#: KioImportWindow.cpp:82
#, kde-format
msgid "A tool to import images over network"
msgstr "Una eina per a importar imatges per la xarxa"

#: KioImportWindow.cpp:137
#, kde-format
msgid "Import not completed"
msgstr "No s'ha completat la importació"

#: KioImportWindow.cpp:138
#, kde-format
msgid ""
"Some of the images have not been transferred and are still in the list. You "
"can retry to import these images now."
msgstr ""
"Algunes de les imatges no s'han transferit i encara es troben en la llista. "
"Ara podeu tornar a intentar importar-les."

#. i18n: ectx: Menu (Import)
#: kipiplugin_remotestorageui.rc:7
#, kde-format
msgid "&Import"
msgstr "&Importa"

#. i18n: ectx: Menu (Export)
#: kipiplugin_remotestorageui.rc:11
#, kde-format
msgid "&Export"
msgstr "&Exporta"

#. i18n: ectx: ToolBar (mainToolBar)
#: kipiplugin_remotestorageui.rc:17
#, kde-format
msgid "Main Toolbar"
msgstr "Barra d'eines principal"

#: plugin_remotestorage.cpp:93
#, kde-format
msgid "Export to remote storage..."
msgstr "Exporta cap a un emmagatzematge remot…"

#: plugin_remotestorage.cpp:105
#, kde-format
msgid "Import from remote storage..."
msgstr "Importa des d'un emmagatzematge remot…"
