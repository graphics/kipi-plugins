# Translation of kipiplugin_printimages.po to Catalan (Valencian)
# Copyright (C) 2009-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Joan Maspons <joanmaspons@gmail.com>, 2009.
# Josep M. Ferrer <txemaq@gmail.com>, 2010, 2012, 2014, 2015, 2016, 2017, 2019.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2012, 2013, 2014, 2015, 2017, 2020, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kipi-plugins\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:26+0000\n"
"PO-Revision-Date: 2023-02-22 21:50+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca@valencia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#. i18n: ectx: Menu (Image)
#: plugin/kipiplugin_printimagesui.rc:7
#, kde-format
msgid "&Image"
msgstr "&Imatge"

#. i18n: ectx: ToolBar (mainToolBar)
#: plugin/kipiplugin_printimagesui.rc:14
#, kde-format
msgid "Main Toolbar"
msgstr "Barra d'eines principal"

#: plugin/plugin_printimages.cpp:105
#, kde-format
msgid "Print images"
msgstr "Impressió de les imatges"

#: plugin/plugin_printimages.cpp:115
#, kde-format
msgid "Print Assistant..."
msgstr "Assistent d'impressió…"

#: plugin/plugin_printimages.cpp:136
#, kde-format
msgid "Print Images"
msgstr "Impressió de les imatges"

#: plugin/plugin_printimages.cpp:136 plugin/plugin_printimages.cpp:156
#, kde-format
msgid "Please select one or more photos to print."
msgstr "Seleccioneu una o més fotografies per a imprimir."

#: plugin/plugin_printimages.cpp:156
#, kde-format
msgid "Print Assistant"
msgstr "Assistent d'impressió"

#: tools/utils.cpp:71
#, kde-format
msgid ""
"Unable to create a temporary folder. Please make sure you have proper "
"permissions to this folder and try again."
msgstr ""
"No s'ha pogut crear una carpeta temporal. Assegureu-vos que teniu els "
"permisos necessaris per a esta carpeta i torneu-ho a provar."

#. i18n: ectx: property (whatsThis), widget (QCheckBox, m_disableCrop)
#: ui/croppage.ui:19
#, kde-format
msgid "Do not crop photos, just scale them."
msgstr "No escapça les fotografies, només canvia l'escala."

#. i18n: ectx: property (text), widget (QCheckBox, m_disableCrop)
#: ui/croppage.ui:22
#, kde-format
msgid "Do not crop"
msgstr "No escapces"

#. i18n: ectx: property (toolTip), widget (QPushButton, BtnCropRotateLeft)
#. i18n: ectx: property (whatsThis), widget (QPushButton, BtnCropRotateLeft)
#: ui/croppage.ui:42 ui/croppage.ui:45
#, kde-format
msgid "Rotate photo to the left"
msgstr "Gira la fotografia cap a l'esquerra"

#. i18n: ectx: property (text), widget (QPushButton, BtnCropRotateLeft)
#: ui/croppage.ui:48
#, kde-format
msgid "Rotate &left"
msgstr "Gira a l'es&querra"

#. i18n: ectx: property (toolTip), widget (QPushButton, BtnCropRotateRight)
#. i18n: ectx: property (whatsThis), widget (QPushButton, BtnCropRotateRight)
#: ui/croppage.ui:55 ui/croppage.ui:58
#, kde-format
msgid "Rotate photo to the right"
msgstr "Gira la fotografia cap a la dreta"

#. i18n: ectx: property (text), widget (QPushButton, BtnCropRotateRight)
#: ui/croppage.ui:61
#, kde-format
msgid "Rotate &right"
msgstr "Gira a la d&reta"

#. i18n: ectx: property (toolTip), widget (QPushButton, BtnCropPrev)
#. i18n: ectx: property (whatsThis), widget (QPushButton, BtnCropPrev)
#. i18n: ectx: property (toolTip), widget (QToolButton, BtnPreviewPageDown)
#. i18n: ectx: property (whatsThis), widget (QToolButton, BtnPreviewPageDown)
#: ui/croppage.ui:84 ui/croppage.ui:87 ui/photopage.ui:287 ui/photopage.ui:290
#, kde-format
msgid "Previous photo"
msgstr "Fotografia anterior"

#. i18n: ectx: property (text), widget (QPushButton, BtnCropPrev)
#: ui/croppage.ui:90
#, kde-format
msgid "<< Pr&evious"
msgstr "<< Ant&erior"

#. i18n: ectx: property (toolTip), widget (QPushButton, BtnCropNext)
#. i18n: ectx: property (whatsThis), widget (QPushButton, BtnCropNext)
#: ui/croppage.ui:97 ui/croppage.ui:100
#, kde-format
msgid "Next photo"
msgstr "Fotografia següent"

#. i18n: ectx: property (text), widget (QPushButton, BtnCropNext)
#: ui/croppage.ui:103
#, kde-format
msgid "Ne&xt >>"
msgstr "Següe&nt >>"

#. i18n: ectx: property (text), widget (QLabel, LblCropPhoto)
#: ui/croppage.ui:132
#, kde-format
msgid "Photo 0 of 0"
msgstr "Fotografia 0 de 0"

#. i18n: ectx: property (whatsThis), widget (KIPIPrintImagesPlugin::CropFrame, cropFrame)
#: ui/croppage.ui:155
#, kde-format
msgid ""
"Move the box in order to crop photos so that they fit inside the photo sizes "
"you have given.  \n"
"You can crop each image differently, or just click the 'Next' button to use "
"the default crop \n"
"setting for each photo.\n"
"Enable \"Do not crop\" to avoid cropping all of the photos."
msgstr ""
"Moveu el quadro per tal d'escapçar les fotografies i que encaixen dins la "
"mida de fotografia que heu especificat.  \n"
"Podeu escapçar cada imatge per separat o clicar el botó «Següent» per a "
"utilitzar les configuracions d'escapçat \n"
"per defecte per a cada fotografia.\n"
"Marqueu l'opció «No escapces» per a evitar escapçar totes les fotografies."

#. i18n: ectx: property (text), widget (QPushButton, BtnSaveAs)
#: ui/croppage.ui:167
#, kde-format
msgid "Save As"
msgstr "Anomena i guarda"

#. i18n: ectx: property (shortcut), widget (QPushButton, BtnSaveAs)
#: ui/croppage.ui:170
#, kde-format
msgid "Ctrl+S"
msgstr "Ctrl+S"

#. i18n: ectx: property (windowTitle), widget (QDialog, CustomLayout)
#: ui/customlayout.ui:26 wizard/wizard.cpp:711
#, kde-format
msgid "Custom layout"
msgstr "Disposició personalitzada"

#. i18n: ectx: property (text), widget (QRadioButton, m_photoGridCheck)
#: ui/customlayout.ui:48
#, kde-format
msgid "Photo grid"
msgstr "Quadrícula de les fotografies"

#. i18n: ectx: property (text), widget (QRadioButton, m_fitAsManyCheck)
#: ui/customlayout.ui:73
#, kde-format
msgid "Fit as many as possible"
msgstr "Ajusta al màxim nombre possible"

#. i18n: ectx: property (text), widget (QRadioButton, m_photosXPageCheck)
#: ui/customlayout.ui:95
#, kde-format
msgid "Photos per page"
msgstr "Fotografies per pàgina"

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/customlayout.ui:122
#, kde-format
msgid "Rows"
msgstr "Files"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/customlayout.ui:164
#, kde-format
msgid "Columns"
msgstr "Columnes"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: ui/customlayout.ui:227
#, kde-format
msgid "Photo size"
msgstr "Mida de les fotografies"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: ui/customlayout.ui:267 ui/printoptionspage.ui:439
#, kde-format
msgid "x"
msgstr "x"

#. i18n: ectx: property (text), item, widget (QComboBox, m_photoUnits)
#: ui/customlayout.ui:309 wizard/wizard.cpp:2048
#, kde-format
msgid "cm"
msgstr "cm"

#. i18n: ectx: property (text), item, widget (QComboBox, m_photoUnits)
#: ui/customlayout.ui:314
#, kde-format
msgid "mm"
msgstr "mm"

#. i18n: ectx: property (text), item, widget (QComboBox, m_photoUnits)
#: ui/customlayout.ui:319 wizard/wizard.cpp:2043
#, kde-format
msgid "inches"
msgstr "polzades"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_PrintAutoRotate)
#. i18n: ectx: property (text), widget (QCheckBox, m_autorotate)
#: ui/customlayout.ui:433 ui/printoptionspage.ui:73
#, kde-format
msgid "Auto rotate"
msgstr "Gir automàtic"

#. i18n: ectx: property (text), widget (QPushButton, m_doneButton)
#: ui/customlayout.ui:453
#, kde-format
msgid "Done"
msgstr "Fet"

#. i18n: ectx: property (text), widget (QLabel, TextLabel4_2)
#: ui/photopage.ui:47
#, kde-format
msgid "Photos:"
msgstr "Fotografies:"

#. i18n: ectx: property (text), widget (QLabel, LblPhotoCount)
#. i18n: ectx: property (text), widget (QLabel, LblSheetsPrinted)
#. i18n: ectx: property (text), widget (QLabel, LblEmptySlots)
#: ui/photopage.ui:54 ui/photopage.ui:84 ui/photopage.ui:114
#, kde-format
msgid "<p align=\"right\">0</p>"
msgstr "<p align=\"right\">0</p>"

#. i18n: ectx: property (text), widget (QLabel, TextLabel4)
#: ui/photopage.ui:77
#, kde-format
msgid "Sheets:"
msgstr "Fulls:"

#. i18n: ectx: property (text), widget (QLabel, TextLabel6)
#: ui/photopage.ui:107
#, kde-format
msgid "Empty slots:"
msgstr "Ranures buides:"

#. i18n: ectx: property (text), item, widget (QComboBox, m_printer_choice)
#: ui/photopage.ui:185 wizard/wizard.cpp:1800 wizard/wizard.cpp:2284
#, kde-format
msgid "Print to PDF"
msgstr "Imprimix a PDF"

#. i18n: ectx: property (text), item, widget (QComboBox, m_printer_choice)
#: ui/photopage.ui:190 wizard/wizard.cpp:1801 wizard/wizard.cpp:2242
#: wizard/wizard.cpp:2312 wizard/wizard.cpp:2515 wizard/wizard.cpp:2587
#, kde-format
msgid "Print to JPG"
msgstr "Imprimix a JPG"

#. i18n: ectx: property (text), item, widget (QComboBox, m_printer_choice)
#: ui/photopage.ui:195 wizard/wizard.cpp:1802 wizard/wizard.cpp:2516
#: wizard/wizard.cpp:2560
#, kde-format
msgid "Print to gimp"
msgstr "Imprimix a «gimp»"

#. i18n: ectx: property (text), widget (QPushButton, m_pagesetup)
#: ui/photopage.ui:209
#, kde-format
msgid "Page settings"
msgstr "Configureu la pàgina"

#. i18n: ectx: property (text), widget (QPushButton, mLeftButton)
#. i18n: ectx: property (text), widget (QToolButton, BtnPreviewPageDown)
#: ui/photopage.ui:293 ui/printoptionspage.ui:57
#, kde-format
msgid "<"
msgstr "<"

#. i18n: ectx: property (text), widget (QLabel, LblPreview)
#: ui/photopage.ui:315
#, kde-format
msgid "Preview"
msgstr "Vista prèvia"

#. i18n: ectx: property (toolTip), widget (QToolButton, BtnPreviewPageUp)
#. i18n: ectx: property (whatsThis), widget (QToolButton, BtnPreviewPageUp)
#: ui/photopage.ui:331 ui/photopage.ui:334
#, kde-format
msgid "Next page"
msgstr "Pàgina següent"

#. i18n: ectx: property (text), widget (QPushButton, mRightButton)
#. i18n: ectx: property (text), widget (QToolButton, BtnPreviewPageUp)
#: ui/photopage.ui:337 ui/printoptionspage.ui:64
#, kde-format
msgid ">"
msgstr ">"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/photopage.ui:436
#, kde-format
msgid "Captions:"
msgstr "Comentaris:"

#. i18n: ectx: property (text), item, widget (QComboBox, m_captions)
#: ui/photopage.ui:447 wizard/wizard.cpp:1898
#, kde-format
msgid "No captions"
msgstr "Sense comentaris"

#. i18n: ectx: property (text), item, widget (QComboBox, m_captions)
#: ui/photopage.ui:452
#, kde-format
msgid "Image file names"
msgstr "Noms dels fitxers d'imatge"

#. i18n: ectx: property (text), item, widget (QComboBox, m_captions)
#: ui/photopage.ui:457
#, kde-format
msgid "Exif date-time"
msgstr "Data-hora Exif"

#. i18n: ectx: property (text), item, widget (QComboBox, m_captions)
#: ui/photopage.ui:462
#, kde-format
msgid "Comments"
msgstr "Comentaris"

#. i18n: ectx: property (text), item, widget (QComboBox, m_captions)
#: ui/photopage.ui:467 wizard/wizard.cpp:1904
#, kde-format
msgid "Free"
msgstr "Lliure"

#. i18n: ectx: property (text), widget (QLabel, m_free_label)
#: ui/photopage.ui:501
#, no-c-format, kde-format
msgid ""
"%f  filename, %t exposure time, %c comment, %i ISO, %d date-time, %r "
"resolution, %a aperture, %l focal length, \\n newline"
msgstr ""
"%f nom de fitxer, %t temps d'exposició, %c comentari, %i ISO, %d data-hora, "
"%r resolució, %a obertura, %l distància focal, \\n línia nova"

#. i18n: ectx: property (text), widget (QCheckBox, m_sameCaption)
#: ui/photopage.ui:532
#, kde-format
msgid "same to all"
msgstr "mateix a tot"

#. i18n: ectx: property (text), widget (QPushButton, mSaveSettings)
#. i18n: ectx: property (text), widget (QPushButton, m_setDefault)
#: ui/photopage.ui:542 ui/printoptionspage.ui:348
#, kde-format
msgid "Use as default"
msgstr "Utilitza com a predeterminat"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: ui/photopage.ui:555
#, kde-format
msgid "Family:"
msgstr "Família:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: ui/photopage.ui:572
#, kde-format
msgid "Color:"
msgstr "Color:"

#. i18n: ectx: property (text), widget (QLabel, textLabel1_2)
#: ui/photopage.ui:596
#, kde-format
msgid "Size:"
msgstr "Mida:"

#. i18n: ectx: property (windowTitle), widget (QWidget, PrintOptionsPage)
#: ui/printoptionspage.ui:14
#, kde-format
msgid "Image Settings"
msgstr "Configureu la imatge"

#. i18n: ectx: property (title), widget (QGroupBox, mGroupImage)
#: ui/printoptionspage.ui:83
#, kde-format
msgid "Position"
msgstr "Posició"

#. i18n: ectx: property (title), widget (QGroupBox, mGroupScaling)
#: ui/printoptionspage.ui:108
#, kde-format
msgid "Scaling"
msgstr "Canvi de mida"

#. i18n: ectx: property (text), widget (QRadioButton, mNoScale)
#: ui/printoptionspage.ui:114
#, kde-format
msgid "&No scaling"
msgstr "Se&nse canvi de mida"

#. i18n: ectx: property (text), widget (QRadioButton, mScaleToPage)
#: ui/printoptionspage.ui:124
#, kde-format
msgid "&Fit image to page"
msgstr "&Ajusta la imatge a la pàgina"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_PrintEnlargeSmallerImages)
#: ui/printoptionspage.ui:155
#, kde-format
msgid "Enlarge smaller images"
msgstr "Augmenta les imatges xicotetes"

#. i18n: ectx: property (text), widget (QRadioButton, mScaleTo)
#: ui/printoptionspage.ui:180
#, kde-format
msgid "&Scale to:"
msgstr "Canvia de &mida a:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: ui/printoptionspage.ui:219
#, kde-format
msgctxt "Dimension separator, as in: '15 x 10 centimeters'"
msgid "x"
msgstr "x"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_PrintUnit)
#: ui/printoptionspage.ui:246
#, kde-format
msgid "Millimeters"
msgstr "Mil·límetres"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_PrintUnit)
#: ui/printoptionspage.ui:251
#, kde-format
msgid "Centimeters"
msgstr "Centímetres"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_PrintUnit)
#: ui/printoptionspage.ui:256
#, kde-format
msgid "Inches"
msgstr "Polzades"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_PrintKeepRatio)
#: ui/printoptionspage.ui:300
#, kde-format
msgid "Keep ratio"
msgstr "Mantín les proporcions"

#. i18n: ectx: property (toolTip), widget (QPushButton, mSaveSettings)
#: ui/printoptionspage.ui:345
#, kde-format
msgid "Use these scaling options as default. "
msgstr "Utilitza estes opcions d'escalat com a predeterminades. "

#. i18n: ectx: property (text), widget (QLabel, label)
#: ui/printoptionspage.ui:366
#, kde-format
msgid "Photos per page:"
msgstr "Fotografies per pàgina:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: ui/printoptionspage.ui:376
#, kde-format
msgid "Multiple pages:"
msgstr "Múltiples pàgines:"

#: wizard/customdlg.cpp:51 wizard/customdlg.cpp:52
#, kde-format
msgid "Choose your grid size"
msgstr "Seleccioneu la mida de la quadrícula"

#: wizard/customdlg.cpp:53
#, kde-format
msgid "Number of rows"
msgstr "Nombre de files"

#: wizard/customdlg.cpp:54
#, kde-format
msgid "Insert number of rows"
msgstr "Inseriu el nombre de files"

#: wizard/customdlg.cpp:55
#, kde-format
msgid "Number of columns"
msgstr "Nombre de columnes"

#: wizard/customdlg.cpp:56
#, kde-format
msgid "Insert number of columns"
msgstr "Inseriu el nombre de columnes"

#: wizard/customdlg.cpp:58 wizard/customdlg.cpp:59
#, kde-format
msgid "Choose to have a custom photo size album"
msgstr ""
"Seleccioneu per a tindre un àlbum de fotografies amb mida personalitzada"

#: wizard/customdlg.cpp:60
#, kde-format
msgid "Photo height"
msgstr "Alçària de la fotografia"

#: wizard/customdlg.cpp:61
#, kde-format
msgid "Insert photo height"
msgstr "Inseriu l'alçària de la fotografia"

#: wizard/customdlg.cpp:62
#, kde-format
msgid "Photo width"
msgstr "Amplària de la fotografia"

#: wizard/customdlg.cpp:63
#, kde-format
msgid "Insert photo width"
msgstr "Inseriu l'amplària de la fotografia"

#: wizard/customdlg.cpp:65
#, kde-format
msgid "Auto rotate photo"
msgstr "Gir automàtic de la fotografia"

#: wizard/printhelper.cpp:241
#, kde-format
msgid "Kipi-plugins image printing"
msgstr "Connector Kipi d'impressió d'imatges"

#: wizard/printoptionspage.cpp:149 wizard/printoptionspage.cpp:150
#: wizard/printoptionspage.cpp:151
#, kde-format
msgid "disabled"
msgstr "desactivat"

#: wizard/wizard.cpp:117
#, kde-format
msgid "Select page layout"
msgstr "Seleccioneu la disposició de la pàgina"

#: wizard/wizard.cpp:118
#, kde-format
msgid "Crop photos"
msgstr "Escapça les fotografies"

#: wizard/wizard.cpp:120
#, kde-format
msgid "Custom"
msgstr "Personalitzat"

#: wizard/wizard.cpp:162 wizard/wizard.cpp:165 wizard/wizard.cpp:2531
#, kde-format
msgid "Print assistant"
msgstr "Assistent d'impressió"

#: wizard/wizard.cpp:166
#, kde-format
msgid "A tool to print images"
msgstr "Una eina per a imprimir imatges"

#: wizard/wizard.cpp:167
#, kde-format
msgid ""
"(c) 2003-2004, Todd Shoemaker\n"
"(c) 2007-2013, Angelo Naselli"
msgstr ""
"(c) 2003-2004, Todd Shoemaker\n"
"(c) 2007-2013, Angelo Naselli"

#: wizard/wizard.cpp:170
#, kde-format
msgid "Todd Shoemaker"
msgstr "Todd Shoemaker"

#: wizard/wizard.cpp:171
#, kde-format
msgid "Author"
msgstr "Autoria"

#: wizard/wizard.cpp:174
#, kde-format
msgid "Angelo Naselli"
msgstr "Angelo Naselli"

#: wizard/wizard.cpp:175
#, kde-format
msgid "Developer"
msgstr "Desenvolupador"

#: wizard/wizard.cpp:178
#, kde-format
msgid "Andreas Trink"
msgstr "Andreas Trink"

#: wizard/wizard.cpp:179
#, kde-format
msgid "Contributor"
msgstr "Col·laborador"

#: wizard/wizard.cpp:678
#, kde-format
msgid "Unsupported Paper Size"
msgstr "Mida del paper no acceptada"

#: wizard/wizard.cpp:1172
#, kde-format
msgid "Photo %1 of %2"
msgstr "Fotografia %1 de %2"

#: wizard/wizard.cpp:1256 wizard/wizard.cpp:1263
#, kde-format
msgid "Page %1 of %2"
msgstr "Pàgina %1 de %2"

#: wizard/wizard.cpp:1504
#, kde-format
msgid "Add again"
msgstr "Afig una altra vegada"

#: wizard/wizard.cpp:1514
#, kde-format
msgid "Remove"
msgstr "Elimina"

#: wizard/wizard.cpp:2218
#, kde-format
msgid "Output Path"
msgstr "Camí de l'eixida"

#: wizard/wizard.cpp:2413
#, kde-format
msgid "Overwrite File"
msgstr "Sobreescriu el fitxer"

#: wizard/wizard.cpp:2414
#, kde-format
msgid ""
"The following file will be overwritten. Are you sure you want to overwrite "
"it?"
msgstr "El fitxer següent serà sobreescrit. Segur que voleu sobreescriure'l?"

#: wizard/wizard.cpp:2438
#, kde-format
msgid "Could not save file, please check your output entry."
msgstr "No s'ha pogut guardar el fitxer, comproveu el resultat de l'entrada."

#: wizard/wizard.cpp:2462
#, kde-format
msgid "Could not remove the GIMP's temporary files."
msgstr "No s'han pogut eliminar els fitxers temporals de GIMP."

#: wizard/wizard.cpp:2583
#, kde-format
msgid ""
"There was an error launching the GIMP. Please make sure it is properly "
"installed."
msgstr ""
"S'ha produït un error en iniciar GIMP. Assegureu-vos que està instal·lat "
"correctament."

#: wizard/wizard.cpp:2595
#, kde-format
msgid "Empty output path."
msgstr "Camí de l'eixida buit."
